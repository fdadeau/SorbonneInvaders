/**
 *  Projectiles.js
 */
function Projectiles(_characters, _game) {
 
    // total number of projectiles
    var NBPROJECTILES = 5;
    // pool of projectiles
    var projectiles = [];
    // initialisation
    for (var i=0; i < NBPROJECTILES; i++) {
        projectiles[i] = { active: false, kind: 0, state: 0, speed: 0.2, vecX: 0, vecY: 0, x: 0, y: 0, w: 18, h: 18, idx: 0, last: 0 };
    }
    // current number of active projectiles
    var active = 0;
    
    var anim = [ 
        {srcX: 0,  srcY: 0, srcW: 32, srcH: 32}, 
        {srcX: 32, srcY: 0, srcW: 32, srcH: 32}, 
        {srcX: 64, srcY: 0, srcW: 32, srcH: 32}, 
        {srcX: 96, srcY: 0, srcW: 32, srcH: 32}, 
        {srcX: 64, srcY: 0, srcW: 32, srcH: 32}, 
        {srcX: 32, srcY: 0, srcW: 32, srcH: 32}, 
    ];
    var pave = new Image();
    pave.src = "./images/pave.png";
    
    
    this.reInit = function() {
        active = 0;   
        for (var i=0; i < NBPROJECTILES; i++) {
            projectiles[i].active = false;
        }
    }
    
    this.shoot = function(_x, _y, _k, _s) {
        if (_k == 0 && active > 0) {
            return;
        }
        for (var i=0; i < NBPROJECTILES; i++) {
            if (! projectiles[i].active) {
                projectiles[i].active = true;
                projectiles[i].kind = _k;
                projectiles[i].x = _x;
                projectiles[i].y = _y;
                projectiles[i].speed = 0.45;
                projectiles[i].vecY = _s.y;
                projectiles[i].vecX = _s.x;
                projectiles[i].idx = 0;
                active++;
                return;
            }
        }
    }
    
    this.update = function(timer) {
        for (var i=0; i < NBPROJECTILES; i++) {
            var p = projectiles[i];
            if (p.active) {
                p.y += p.speed * timer.delta * p.vecY;
                p.x += p.speed * timer.delta * p.vecX;
                if (timer.now - p.last > 200) {
                    p.last = timer.now;
                    p.idx++;
                    if (p.idx >= anim.length) {
                        p.idx = 0;   
                    }
                }
                if (_characters.collision(p)) {
                    p.active = false;
                    active--;
                }
                else if (p.y < 0 || p.x < 0 || p.x > _game.width || p.y > _game.height) {
                    p.active = false;
                    active--;
                }
            }
        }
    }
    
    var f = 2;
    this.render = function(ctx) {
        ctx.drawImage(pave, anim[0].srcX, anim[0].srcY, anim[0].srcW, anim[0].srcH, 10, _game.height - 74, 50, 50); 
        for (var i=0; i < NBPROJECTILES; i++) {
            var p = projectiles[i];
            if (p.active) {
                ctx.drawImage(pave, anim[p.idx].srcX, anim[p.idx].srcY, anim[p.idx].srcW, anim[p.idx].srcH, 
                              p.x - p.w*f*0.5, p.y - p.h*f*0.5, p.w*f, p.h*f);
            }
        }
    }
    
}