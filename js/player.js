function Player(_x, _y, _game) {
 
    /**
     *  Class representing the player
     */
    this.x = _x;
    this.y = _y;
    
    /** Player size */
    this.width = 64;
    this.height = 64;
    
    /** Player position & movement */
    this.minX = _x - _x * 0.7;
    this.maxX = _x + _x * 0.7;
    this.vecX = 0;
    this.speed = 0.3;
        
    var STARTAMMO = 30;
    
    /** Player weapon */
    this.weapon = 0;
    this.ammo = STARTAMMO;
    
    this.spritesheet = new Image();
    this.spritesheet.src = "./images/player.png";
    
    this.shooting = null;
    
    this.score = 0;
    
    /**
     *  Reinitialization
     */
    this.reInit = function() {
        this.x = _x;
        this.y = _y;
        this.vecX = 0;
        this.weapon = 0;
        this.ammo = STARTAMMO;
        this.score = 0;
    }
    
    /** 
     *  Update player
     */
    this.update = function(timer) {
        // player movement
        var newX = this.x + this.vecX * timer.delta * this.speed;
        if (newX >= this.minX && newX <= this.maxX) {
            this.x = newX;   
        }
        
        // player animation
        if (this.vecX == 0) { 
            this.animBas = bas.idle;
        }
        else {
            if (timer.now - bas.last > 70) {
                bas.last = timer.now;
                bas.idx += this.vecX;
                if (bas.idx < 0) {
                    bas.idx = bas.walk.length - 1;
                }
                else if (bas.idx >= bas.walk.length) {
                    bas.idx = 0;
                }
                this.animBas = bas.walk[bas.idx];
            }
        }
            
        if (this.shooting) {
            if (timer.now - haut.last > 90) {
                haut.last = timer.now;
                haut.idx += 1;
                if (haut.idx >= haut.throw.length) {
                    this.shooting = null;
                    this.animHaut = haut.idle;
                    haut.idx = -1;
                }
                else if (haut.idx == 4) {
                    if (this.ammo > 0) {
                        this.ammo--;
                        _game.shoot(this.x + this.width*0.3, this.y, 0, this.shooting);
                    }
                }
                else {
                    this.animHaut = haut.throw[haut.idx];
                }
                
            }
        }
    }
    
    
    this.incMunitions = function(nb) {
        this.ammo += nb;       
    }
    this.incScore = function(nb) {
        this.score += nb;
    }
    
    this.shoot = function(_x, _y) {
        if (this.shooting != null) return;
        if (this.ammo == 0) return;
        haut.idx = -1;
        this.shooting = {x: _x, y: _y};
    }
    
    
    var haut = { idle: { srcX: 0, srcY: 0, srcW: 64, srcH: 64 }, 
                 throw: [ 
                     { srcX: 65, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 130, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 195, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 260, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 324, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 389, srcY: 0, srcW: 64, srcH: 64 },
                     { srcX: 454, srcY: 0, srcW: 64, srcH: 64 }
                 ],
                 idx: 0,
                 last: 0
               };
    var bas = { idle: { srcX: 0, srcY: 64, srcW: 64, srcH: 64 },
                walk: [
                     { srcX: 65, srcY: 64, srcW: 64, srcH: 64 },
                     { srcX: 130, srcY: 64, srcW: 64, srcH: 64 },
                     { srcX: 195, srcY: 64, srcW: 64, srcH: 64 },
                     { srcX: 260, srcY: 64, srcW: 64, srcH: 64 }
                ],
               idx: 0,
               last: 0
              };
    
    
    this.animHaut = haut.idle;
    this.animBas = bas.idle;
    
    var f = 2.2;
    
    /**
     *  Rendering
     */     
    this.render = function(ctx) {
        
        ctx.drawImage(this.spritesheet, this.animHaut.srcX, this.animHaut.srcY, this.animHaut.srcW, this.animHaut.srcH, this.x - this.width * f / 2, this.y - this.height * f / 2, this.width * f, this.height * f);
        ctx.drawImage(this.spritesheet, this.animBas.srcX, this.animBas.srcY, this.animBas.srcW, this.animBas.srcH, this.x - this.width * f / 2, this.y - this.height * f / 2, this.width * f, this.height * f);
        
        ctx.font = "24px Arial";
        ctx.fillStyle = "white";
        ctx.fillText(this.ammo, 40, _game.height - 10);
        ctx.fillText("Score : " + this.score, 10, 24);
    }
    
    
    
    
}



