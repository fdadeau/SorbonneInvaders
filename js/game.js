document.addEventListener("DOMContentLoaded", function(e) {


    // initialisation des zones
    var w = window.innerWidth; // * 0.9;
    var h = window.innerHeight; // * 0.9;
    
    cvs.height = h;
    cvs.width = w;
        
    var ctx = cvs.getContext("2d");
    
    
    (function Game() {
    
        this.width = w;
        this.height = h;
        
        // leader
        this.leader = new Player(w*0.5, h*0.85, this);
        // amphi --> contains movement paths
        this.amphi = new Amphi(w, h);
        // personnages 
        this.personnages = new Characters(amphi, this);
        // projectiles
        this.projectiles = new Projectiles(personnages, this);
    
        this.audio = new MyAudio();
        
        /** 
         *  Reinitialisation
         */
        this.reInit = function() {
            this.leader.reInit();
            this.personnages.reInit();
            this.projectiles.reInit();
        }

        /**
        *  Timer : gestion du delta de temps. 
        */
        var timer = {
            now: Date.now(),
            last: this.now,
            delta: 0,
            tick: function() {
                this.last = this.now;
                this.now = Date.now();
                this.delta = this.now - this.last;
            }
        }
    
        /** 
         *  Update at a given time interval
         */
        this.update = function(timer) {
            this.leader.update(timer);
            this.projectiles.update(timer);   
            this.personnages.update(timer);
        }
    
        /**
         *  Render
         */
        this.render = function() {
            ctx.clearRect(0, 0, cvs.width, cvs.height);
            
            this.amphi.render(ctx);
            // dessin personnages
            this.personnages.render(ctx);
            // dessin projectiles
            this.projectiles.render(ctx);
            // dessin personnage
            this.leader.render(ctx);
        }
    
    
        /** 
         *  IHM function
         */    
        this.start = function() {
            this.reInit();
            this.paused = false;
            document.getElementById("bcTitle").style.display = "none";
            document.getElementById("bcGameOver").style.display = "none";
            document.getElementById("bcPause").style.display = "none";
            document.getElementById("bcInfos").style.display = "none";
            document.getElementById("bcMain").style.display = "block";
        }
        this.infos = function() {
            this.paused = true;
            document.getElementById("bcTitle").style.display = "none";
            document.getElementById("bcGameOver").style.display = "none";
            document.getElementById("bcInfos").style.display = "block";
            document.getElementById("bcPause").style.display = "none";
            document.getElementById("bcMain").style.display = "none";
        }
        this.quit = function() {
            this.paused = true;
            document.getElementById("bcTitle").style.display = "block";
            document.getElementById("bcInfos").style.display = "none";
            document.getElementById("bcGameOver").style.display = "none";
            document.getElementById("bcPause").style.display = "none";
            document.getElementById("bcMain").style.display = "none";
        }

        
        this.paused = true;
        this.boucleDeJeu = function() {
            requestAnimationFrame(this.boucleDeJeu);
            timer.tick();
            if (!this.paused) {
                this.update(timer);
                this.render();
            }
        }
        this.boucleDeJeu();
        // this.start();
    

        /**
         *  Game actions
         */
        // shoot
        this.shoot = function(_x, _y, _k, _s) {
            projectiles.shoot(_x, _y, _k, _s);       
        }
        // personnage ami arrive en bas de l'amphi
        this.incrementMunitions = function(nb) {
            leader.incMunitions(nb);
            leader.incScore(100);
        }
        // incrément du score
        this.scoring = function(nb) {
            leader.incScore(nb);   
        }
        // personnage ennemi arrive en bas de l'amphi
        this.gameover = function() {
            this.paused = true;
            document.getElementById("bcGameOver").style.display = "block";
        }
                
        
        var that = this;
        /** 
         *  Controller
         */
        document.addEventListener("keydown", function(e) { 
            switch (e.keyCode) {
                case 37: 
                    that.leader.vecX = -1;      
                    break;
                case 39:
                    that.leader.vecX = 1;
                    break;
                case 32:    // touche espace
                    that.leader.shoot(0, -1);
                    break;
                case 27:
                case 80:    // pause
                    that.paused = !that.paused;
                    document.getElementById("bcPause").style.display = (that.paused) ? "block" : "none";
                    break;

            }
        }, false);

        document.addEventListener("keyup", function(e) {
            switch (e.keyCode) {
                case 37: 
                    if (that.leader.vecX < 0) {
                        that.leader.vecX = 0;   
                    }
                    break;
                case 39:
                    if (that.leader.vecX > 0) {
                        that.leader.vecX = 0;   
                    }
                    break;
            }
        }, false);
        
        document.getElementById("cvs").addEventListener("click", function(event) {
            if (event.target.id == "cvs") {
                var x = event.pageX - event.target.offsetLeft;
                var y = event.pageY - event.target.offsetTop;
                var dist = Math.sqrt((that.leader.x - x)*(that.leader.x - x)+(that.leader.y - y)*(that.leader.y - y));
                that.leader.shoot((x-that.leader.x)/dist, (y-that.leader.y)/dist);
            }
        }, false);
            
        document.getElementById("btnStart").addEventListener("click", that.start.bind(that), false);
        document.getElementById("btnQuit").addEventListener("click", that.quit.bind(that), false);
        document.getElementById("btnQuit2").addEventListener("click", that.quit.bind(that), false);
        document.getElementById("btnQuit3").addEventListener("click", that.quit.bind(that), false);
        document.getElementById("btnInfos").addEventListener("click", that.infos.bind(that), false);
        
    })();
    
}, false);