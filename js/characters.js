/**
 *  Class that manages the set of characters that go through the amphi. 
 */
function Characters(_amphi, _game) {
 
    // number of characters
    var NBCHARACTERS = 20;
    // set of characters
    var characters = [];
    
    // initialization 
    for (var i=0; i < NBCHARACTERS; i++) {
        characters[i] = { active: 0, distance: 0, speed: 1, kind: 0, w: 30, h: 50, x: 0, y: 0, idx: 0, last: 0, f: 1.7 }    
    }
    delayToNextSpawn = Math.random() * 4000 + 1000;
    
    
    // ressources graphiques
    var charGFX = [new Image(), new Image(), new Image(), new Image()];
    charGFX[0].src = "./images/crs.png";
    charGFX[1].src = "./images/skin.png";
    charGFX[2].src = "./images/anar.png";
    charGFX[3].src = "./images/peace.png";
    var anim = [ { srcX: 0,   srcY: 0, srcW: 64, srcH: 64 },
                 { srcX: 64,  srcY: 0, srcW: 64, srcH: 64 },
                 { srcX: 128, srcY: 0, srcW: 64, srcH: 64 },
                 { srcX: 64,  srcY: 0, srcW: 64, srcH: 64 }
               ];
    var pofbam = new Image();
    pofbam.src = "./images/paf.png";
    var pof = [ 
        { srcX: 0,   srcY: 0, srcW: 64, srcH: 64 }, 
        { srcX: 64,  srcY: 0, srcW: 64, srcH: 64 }, 
        { srcX: 128, srcY: 0, srcW: 64, srcH: 64 } 
    ];
    var bam = [ 
        { srcX: 0,   srcY: 64, srcW: 64, srcH: 64 }, 
        { srcX: 64,   srcY: 64, srcW: 64, srcH: 64 }, 
        { srcX: 128,   srcY: 64, srcW: 64, srcH: 64 } 
    ];
    var scoring = [200, 300, -100, -100];
    

    this.reInit = function() {
        for (var i=0; i < NBCHARACTERS; i++) {
            characters[i].active = 0;
        }
        delayToNextSpawn = Math.random()*4000 + 1000; 
    }
    
    this.spawn = function() {
        for (var i=0; i < NBCHARACTERS; i++) {
            if (! characters[i].active) {
                characters[i].active = 1;
                characters[i].distance = _amphi.getLength();
                characters[i].kind = Math.random() * 4 | 0;
                characters[i].gfx = charGFX[characters[i].kind];
                characters[i].speed = Math.random() * 0.2 + 0.05;
                characters[i].idx = 0;
                characters[i].score = scoring[characters[i].kind];
                characters[i].ammo = (Math.random() * 3) | 0 + 4;
                return;
            }
        }
    }
    
    var delayToNextSpawn = 0;
    
    this.update = function(timer) {
        // respawn
        delayToNextSpawn -= timer.delta;
        if (delayToNextSpawn < 0) {
            this.spawn();
            delayToNextSpawn = Math.random() * 4000 + 2000;   
        }
        // for each character
        for (var i=0; i < NBCHARACTERS; i++) {
            var c = characters[i];
            switch (c.active) {
                case 1: 
                    // personnage actif : 1 --> en déplacement
                    // animation
                    if (timer.now - c.last > 10 / c.speed) {
                        c.last = timer.now;
                        c.idx++;
                        if (c.idx >= anim.length) {
                            c.idx = 0;   
                        }
                    }
                    c.distance -= c.speed * timer.delta;

                    if (c.kind <= 1) {
                        // rattrapage par la patrouille
                        if (i > 0 && characters[i-1].active == 1 && characters[i-1].kind > 1 && 
                                Math.abs(c.distance - characters[i-1].distance) < 40) {
                            characters[i-1].active = 3;
                            c.active = 11;
                            c.last = timer.now;
                        }
                        if (i < NBCHARACTERS-1 && characters[i+1].active == 1 && characters[i+1].kind > 1 && 
                                Math.abs(c.distance - characters[i+1].distance) < 40) {
                            characters[i+1].active = 3;
                            c.active = 11;
                            c.last = timer.now;
                            _game.audio.playOnce("pifpaf");
                        }
                    }
                    
                    // arrive --> declencher qq chose
                    if (c.distance < 0) {
                        c.active = 0;
                        if (c.kind <= 1) {
                            // 0 ou 1 marine ou black
                            _game.gameover();
                        }   
                        else {
                            _game.incrementMunitions(c.ammo);   
                        }
                    }
                    else {
                        var coords = _amphi.getCoordsFromDistance(characters[i].distance);
                        characters[i].x = coords.x;
                        characters[i].y = coords.y;
                        characters[i].f = 1.7 + (coords.y / _game.height*0.85) * 0.8;
                    }
                    break;
                case 11:
                    if (timer.now - c.last > 1000) {
                        c.last = timer.now;
                        c.active = 1;
                    }
                    break;
                case 2:
                    c.last = timer.now;
                    c.active = 21;
                    break;
                case 21:
                    if (timer.now - c.last > 1000) { 
                        c.last = timer.now;
                        c.active = 0; 
                    }
                    break;
                case 3: 
                    c.last = timer.now;
                    c.active = 31;
                    break;
                case 31: 
                    if (timer.now - c.last > 1000) {
                        c.last = timer.now;
                        c.active = 0;
                    }
                    break;
            }
        }
        characters.sort(function(c1,c2) { return c2.distance - c1.distance; });
    }

    var rangees = [new Image(), new Image(), new Image(), new Image(), new Image()];
    rangees[0].src = "images/rangee5.png";
    rangees[1].src = "images/rangee4.png";
    rangees[2].src = "images/rangee3.png";
    rangees[3].src = "images/rangee2.png";
    rangees[4].src = "images/rangee1.png";

    this.render = function(ctx) {
        var j = 1; 
        ctx.drawImage(rangees[0], 0, 0, width, height);
        for (var i=0; i < NBCHARACTERS; i++) {
            var c = characters[i]; 
            if (c.active > 0) {
                if (j == 1 && c.y > _game.height * 0.12) {
                    ctx.drawImage(rangees[j], 0, 0, width, height); j++;
                }
                if (j == 2 && c.y > _game.height * 0.25) {
                    ctx.drawImage(rangees[j], 0, 0, width, height); j++;
                }
                if (j == 3 && c.y > _game.height * 0.40) {
                    ctx.drawImage(rangees[j], 0, 0, width, height); j++;
                }
                if (j == 4 && c.y > _game.height * 0.54) {
                    ctx.drawImage(rangees[j], 0, 0, width, height); j++;
                }
                
                ctx.drawImage(c.gfx, anim[c.idx].srcX, anim[c.idx].srcY, anim[c.idx].srcW, anim[c.idx].srcH, c.x - 32*c.f, c.y - 32*c.f, 64*c.f, 64*c.f);
                if (c.active == 21) {
                    ctx.drawImage(pofbam, bam[1].srcX, bam[1].srcY, bam[1].srcW, bam[1].srcH, c.x - 32*c.f, c.y - 32*c.f, 64*c.f, 64*c.f);    
                }
                else if (c.active == 31) {
                    ctx.drawImage(pofbam, pof[1].srcX, pof[1].srcY, pof[1].srcW, pof[1].srcH, c.x - 32*c.f, c.y - 32*c.f, 64*c.f, 64*c.f);    
                }
            }
            
        }
        while (j < 5) {
            ctx.drawImage(rangees[j], 0, 0, width, height); j++;
        }
    }
    
    /** 
     *  Triggered when a collision is detected
     */
    this.hit = function(i) {
        _game.audio.playOnce(characters[i].kind == 0 ? "metal" : "boom");
        characters[i].active = 2;   
        _game.scoring(characters[i].score);
    }
    
    /**
     *  Checks collision of the projectile with the characters.
     */
    this.collision = function(p) {
        for (var i=0; i < NBCHARACTERS; i++) {
            var c = characters[i];
            if (c.active != 0) {
                if (!(p.x + p.w/2 < c.x - c.w*c.f/2 || p.x > c.x + c.w*c.f / 2 || p.y + p.w/2 < c.y - c.h*c.f / 4 || p.y > c.y + c.h*c.f/4)) {
                    this.hit(i);
                    return true;   
                }   
            }
        }
        return false;   
    }    
}
 