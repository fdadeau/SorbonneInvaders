/**
 *  Amphi.js
 * 
 *  Paths in the amphitheatre. 
 */
function Amphi(width, height, _game) {
 
    var segments = [
        { x: width*0.08, y: 0 },               // porte entrée 
        { x: width*0.10, y: height*0.10 },      // rang 1 droite
        { x: width*0.92, y: height*0.10 },      // rang 1 gauche
        { x: width*0.90, y: height*0.24 },     // rang 2 gauche
        { x: width*0.12, y: height*0.24 },     // rang 2 droite
        { x: width*0.14, y: height*0.38 },     // rang 3 droite
        { x: width*0.88, y: height*0.38 },     // rang 3 gauche
        { x: width*0.86, y: height*0.52 },     // rang 4 gauche
        { x: width*0.16, y: height*0.52 },     // rang 4 droite
        { x: width*0.18, y: height*0.8 }
    ];
    
    
    var fond = new Image();
    fond.src = "./images/fond.png";
    
    /**
     * Render function.
     */
    this.render = function(ctx){
        ctx.drawImage(fond, 0, 0, width, height);
        /*
        ctx.beginPath();
        ctx.moveTo(segments[0].x, segments[0].y);
        for (var i=0; i < segments.length; i++) {
            ctx.lineTo(segments[i].x, segments[i].y);
        }        
        ctx.stroke();
        */
    }    
    
    /**
     *  Computes the length of the amphi. 
     */
    this.getLength = function() {
        var l = 0;
        var i = 0;
        while (i+1 < segments.length) {
            var d1 = segments[i].x - segments[i+1].x; 
            var d2 = segments[i].y - segments[i+1].y;
            l += Math.sqrt(d1*d1 + d2*d2);
            i++;
        }
        return l;
    }
    
    /**
     *  Computes the coordinates w.r.t. the actual distance. 
     *  distance == 0, end of the track. 
     */
    this.getCoordsFromDistance = function(dist) {
        dist = this.getLength() - dist; 
        // identify segment
        var i=0; 
        var again = 1;
        do {
            var d1 = segments[i].x - segments[i+1].x; 
            var d2 = segments[i].y - segments[i+1].y;
            var segLength = Math.sqrt(d1*d1 + d2*d2);
            again = 0;
            if (dist > segLength) {
                dist -= segLength;   
                i++;
                again = 1;
            }
        }
        while (again);
        
        var ratio = dist / segLength;
        var x = segments[i].x + ((segments[i+1].x - segments[i].x) * ratio);
        var y = segments[i].y + ((segments[i+1].y - segments[i].y) * ratio);
        return {x: x, y: y};
    }
    
    
} 