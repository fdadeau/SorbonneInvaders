/**
 *  Audio.js
 */

function MyAudio() {
    
    var myAudios = { 
        "metal": newAudio("bruitMetal.mp3"), 
        "pifpaf": newAudio("pifpaf.mp3"), 
        "boom": newAudio("coup.mp3")    
    }
    
    function newAudio(src) {
        var r = new Audio();
        r.loop = false;
        r.load();
        r.pause();
        r.src = "./sounds/" + src;
        return r;
    }
    
    this.playOnce = function(which) {
        myAudios[which].play();   
    }
    
}

 